package br.com.deploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeployAutomaticApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(DeployAutomaticApplication.class, args);
	}

}
